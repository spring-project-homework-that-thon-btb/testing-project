//package jpa.tutorial.jpatutorial.controllers;
//
//
//import jpa.tutorial.jpatutorial.entity.Users;
//import jpa.tutorial.jpatutorial.repositories.UserRepositoy;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.security.Principal;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//
//@RestController
//@EnableOAuth2Client
//@RequestMapping("/api")
//@Order(-1)
//public class UserController {
//
//    @Autowired
//    private UserRepositoy userRepositoy;
//
//    @GetMapping("/")
//    public Iterable<Users> index(){
//        return userRepositoy.findAll();
//    }
//
//    @GetMapping("/save")
//    public Iterable<Users> save(){
//        Users users1=new Users("a.b","a.b@gmail.com","123456");
//        Users users2=new Users("a.c","a.c@gmail.com","123456");
//        Users users3=new Users("a.d","a.d@gmail.com","123456");
//
//        List<Users> alluser=new ArrayList<>();
//        alluser.add(users1);
//        alluser.add(users2);
//        alluser.add(users3);
//        userRepositoy.saveAll(alluser);
//        return userRepositoy.findAll();
//    }
//
//    @GetMapping("/user")
//    public Principal principal(Principal principal){
//        Authentication a= SecurityContextHolder.getContext().getAuthentication();
//        HashMap data=(HashMap)((OAuth2Authentication)a).getUserAuthentication().getDetails();
//        String fid=data.get("id").toString();
//        String name=data.get("name").toString();
//        String email=data.get("email").toString();
//        HashMap imgObj=(HashMap) data.get("picture");
//        HashMap imgDetails=(HashMap)imgObj.get("data");
//        String img=imgDetails.get("url").toString();
//        String clientId="281686689250911";
//        return  a;
//    }
//
//    @GetMapping("/login")
//    public String login(){
//        return "login";
//    }
//
//
//}

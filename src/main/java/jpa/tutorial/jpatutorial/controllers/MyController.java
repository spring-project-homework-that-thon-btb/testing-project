package jpa.tutorial.jpatutorial.controllers;

import jpa.tutorial.jpatutorial.entity.Post;
import jpa.tutorial.jpatutorial.entity.User;
import jpa.tutorial.jpatutorial.entity.Users;
import jpa.tutorial.jpatutorial.repositories.PersonRepository;
import jpa.tutorial.jpatutorial.repositories.UserRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RestController
public class MyController {
    @Autowired
    private UserRepositoy userRepositoy;

    @PersistenceContext
    private EntityManager entityManager;

    @GetMapping("/save")
    public Integer saveUsers(){
        Users users=new Users();
        users.setUsername("Userrname");
        users.setEmail("email@gmail.com");
        users.setConfirmPass("confirm");
//        users.setPassword("123456789");
//        users.setRoleId(3);
        return userRepositoy.saveUsers(users);
    }



    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/post")
    public Iterable<Post> user(){
        return personRepository.findAll();
    }
}

package jpa.tutorial.jpatutorial.repositories;

import jpa.tutorial.jpatutorial.entity.Users;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserRepositoy extends CrudRepository<Users,Integer> {

    @Modifying
    @Query(value = "insert into users(username,email,password,note)values(:#{#users.username},:#{#users.email},:#{#users.confirmPass},:#{#users.roleId})",nativeQuery = true)
    public Integer saveUsers(@Param("users") Users users);

}

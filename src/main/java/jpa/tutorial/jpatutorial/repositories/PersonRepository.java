package jpa.tutorial.jpatutorial.repositories;


import jpa.tutorial.jpatutorial.entity.Post;
import jpa.tutorial.jpatutorial.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Post,Integer> {
}
